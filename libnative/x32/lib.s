section .text
	global _start
	global __exit
	global __putstr
	extern main
_start:
	call main
	jmp __exit

__exit:
	;Closes the program
	mov eax, 1
	mov ebx, 0
	int 0x80

	
;THESE FUNCTIONS NEED TO ACCEPT FUNCTIONS m(2,3) = push 3; push 2
; stack, first pop gets pointer, second gets length
; NOT THE PRINT_SINGLE FUNCTION
;save ebx,edi
__putstr:
	; understand these lines of code better
	push ebp
	mov ebp,esp
	sub esp,8
        push ebx
        push edi
	;amke this function save the values of stuff
	mov ecx, [ebp+8]
	mov edx, [ebp+12]


	mov edi,0
	mov eax,4
	mov ebx,1

	int 0x80

        pop edi
        pop ebx
	mov esp,ebp
	pop ebp
	ret
