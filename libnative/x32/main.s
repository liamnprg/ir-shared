section .data
	msg: db "Hello World" 
        db 10
section .text
	global main
	extern __exit
	extern __putstr
main:
        ; push the params on the stack
	push 12
	push msg
	call __putstr
        ; remove the params, we have to do this
        add esp,8
	ret
