.section .text
	.extern __print_single
	.extern __nl
	.extern __putstr
	.global main

main:
	addi sp,sp,-8
	sw ra,0(sp)
        
	lui a0,%hi(msg)
	addi a0,a0,%lo(msg)
	li a1,12
	call __putstr

	lw ra,0(sp)
	addi sp,sp,8
	ret

.section .rodata
msg:
	.string "Hello World\n"
