.section .text
.globl _start
.globl __exit
.globl __putstr
.extern main

_start:
	call main
	j __exit

__exit:
	li a7,94
	li a0, 0x0
	ecall

# do research on how the stack addi and sw works
#address is supposed to be in a0, length in a1
__putstr:
        # We overwrite addresses, so we need to save them on the stack and pop them later
        addi sp,sp,-8
        sw a7,0(sp)
        sw a0,4(sp)

	addi a2,a1,0
	addi a1,a0,0
        li a7,64
        li a0,1

        ecall
        lw a7, 0(sp)
        lb a0, 4(sp)
        addi sp,sp,8

        ret

.section .rodata
