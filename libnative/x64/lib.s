section .data
section .text
	global __exit
	global __putstr
	global _start
	extern main

_start:
	call main
	jmp __exit

__exit:
	;Closes the program
	mov eax, 60
	mov ebx, 0
	syscall

	
;pointer in rax, len in rbx, change the registers used based on what llvm thinks is right, supposed to be  ptr in edi, length in esi
__putstr:
	push rax
	push rsi
	push rdx
	push rdi

	mov rax,1
	mov rdx,rsi
	mov rsi,rdi
	mov rdi,1
	syscall

	pop rdi
	pop rdx
	pop rsi
	pop rax

	ret
