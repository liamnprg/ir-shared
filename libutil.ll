; Declare the string constant as a global constant.
@.newline = private unnamed_addr constant i8 10

; an external assembly routine that prints the first n characters in a string where n is the second i8 argument
declare i32 @__putstr(i8* nocapture,i8) nounwind
; this one is obvious, if it has an __ it's an assembly routine
declare void @__exit() nounwind

; Just prints a newline
define i32 @nl() {  
        ; global variables are automatically pointers to constants, no need to calculate addresses here
	call i32 @__putstr(i8* @.newline ,i8 1)
	ret i32 0
}




; This prints a single digit in a number specified as an i8. Examples include 7,3,5 and 1. You cannot pass any double digit number to this function. See the print_reg function
define i32 @print_single(i8 %x) {  
  ; allocate an i8 on the stack, we use this as the address of the int in the __putstr function
  %ptr = alloca i8
  ; This line makes sense if you look at an ascii table. Do it. If you try to print the literal number 7, what character is that on the ascii table
  %y = add i8 48, %x
  ; once we are done asciifying our stuff, we store the value
  store i8 %y,i8* %ptr
  call i32 @__putstr(i8* %ptr,i8 1)
  ret i32 0
}


define i32 @intprint(i32 %n) {
  %stack = alloca [10 x i8]
  %stackptr = getelementptr inbounds [10 x i8], [10 x i8]* %stack, i32 0, i32 0
  call i32 @intprint_i(i32 %n,i32 0, i32 1,i8* %stackptr)
  ret i32 0
}

;prints in reverse order, fix by pushing to stacc
; u32 has 10 digits
define i32 @intprint_i(i32 %n,i32 %last, i32 %count,i8* %stack) {
first:
;  %tomod = call i32 @llvm.powi.i32(i32 %count,i32 10)
  ;10^count
  %tomod = call i32 @pow(i32 10,i32 %count)

  ;we need this later

  ;read the algo4conv file and the document exmapling this called asciify.txt 
  %temp = urem i32 %n,%tomod
  %0 = sub i32 %temp,%last

  %tomod2 = udiv i32 %tomod,10
  %temp2 = udiv i32 %0,%tomod2

  ;call i32 @print_single(i8 %temp3)

  %cond = icmp eq i32 %last,%n
  br i1 %cond,label %bye, label %after
  

  ; ending block
after:
  %last2 = add i32 %last,%0
  %count2 = add i32 %count,1

  %temp3 = trunc i32 %temp2 to i8
  %countm1 = sub i32 %count,1
  ;%stack2 = insertvalue [10 x i8] %stack, i8 %temp3, 1

  ;add modification to stack here :)

  %extd = sext i32 %countm1 to i64
  %ptr = getelementptr inbounds i8, i8* %stack, i64 %extd
  store i8 %temp3, i8* %ptr

  call i32 @intprint_i(i32 %n,i32 %last2,i32 %count2,i8* %stack)
  ret i32 0
bye:
  ; insert a "print the stack" statement here
  call i32 @print_stack(i8* %stack,i32 %count)
  ret i32 0
}

define i32 @pow(i32 %n,i32 %pow) {
  %cond = icmp eq i32 %pow, 1
  br i1 %cond, label %done,label %notd
notd:
  %y = sub i32 %pow,1
  %1 = call i32 @pow(i32 %n, i32 %y)
  %2 = mul i32 %1,%n
  ret i32 %2
done:
  ret i32 %n
}


define i32 @print_stack(i8* %stack,i32 %count) {
first:
  %i = alloca i32
  ;suppose we have an array as follows: [1,2,3] at memory addresses (0,1,2) respectivly. We are passed a count of 4, meaning we have to sub 2
  %countm1 = sub i32 %count,2
  store i32 %countm1, i32* %i
  br label %continue
continue:
  %reali = load i32, i32* %i

  %ptr = getelementptr i8, i8* %stack, i32 %reali
  %val = load i8,i8* %ptr
  
  call i32 @print_single(i8 %val)
  %cond = icmp eq i32 %reali, 0
  %realim1 = sub i32 %reali,1
  store i32 %realim1, i32* %i

  br i1 %cond,label %done,label %continue
done:
  ret i32 0
}
