# ir-shared

This code contains libnative and some riscv related binaries

## libnative

This is native code for architectures. Currently there are the following architectures

- x86\_64 (stable)
- riscv64 
- x86\_32 (probably unstable but complete)
- riscv32

For more infornation, please consult the llvm-examples repository and the raw assembly code which should contain numerous comments.

Happy compiling :)


