all:
	llvm-as libutil.ll
	cd libnative && make
clean:
	rm libutil.bc
	-cd libnative && make clean
	rm -f *.o
